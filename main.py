# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


import pyfiglet
import sys
import socket
import threading
from datetime import datetime

ascii_banner = pyfiglet.figlet_format("PORT SCANNER")
print(ascii_banner)

# Defining a target
if len(sys.argv) == 2:
    # translate hostname to IPv4
    target = socket.gethostbyname(sys.argv[1])
elif len(sys.argv) == 3:
    #delai avant timeout
    target = socket.gethostbyname(sys.argv[1])
    delay = int(sys.argv[2])
elif len(sys.argv) == 4:
    #nombre de port à scanner
    target = socket.gethostbyname(sys.argv[1])
    delay = int(sys.argv[2])
    nbport = int(sys.argv[3])
else:
    target = ''
    print("Invalid amount of Argument")
    print("arguments : HOSTNAME [DELAY] [NBPORTTOSCAN]")

# Add Banner
print("-" * 50)
print("Scanning Target: " + target)
print("Scanning started at:" + str(datetime.now()))
print("-" * 50)


def TCP_connect(ip, port_number, delay, output):
    TCPsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    TCPsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    TCPsock.settimeout(delay)
    try:
        TCPsock.connect((ip, port_number))
        output[port_number] = 'Listening'
    except:
        output[port_number] = ''


def scan_ports(host_ip, delay=1, nbport=10000):
    threads = []  # To run TCP_connect concurrently
    output = {}  # For printing purposes

    # Spawning threads to scan ports
    for i in range(nbport):
        t = threading.Thread(target=TCP_connect, args=(host_ip, i, delay, output))
        threads.append(t)

    # Starting threads
    for i in range(nbport):
        threads[i].start()

    # Locking the main thread until all threads complete
    for i in range(nbport):
        threads[i].join()

    # Printing listening ports from small to large
    for i in range(nbport):
        if output[i] == 'Listening':
            print("Port {} is open".format(i))

try:

    # will scan ports
    if len(sys.argv) == 2:
    # translate hostname to IPv4
        scan_ports(target)
    elif len(sys.argv) == 3:
        scan_ports(target, delay)
    elif len(sys.argv) == 4:
        scan_ports(target, delay, nbport)

    # for port in range(1, 65535):
    #     s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #     socket.setdefaulttimeout(1)
    #
    #     # returns an error indicator
    #     result = s.connect_ex((target, port))
    #     if result == 0:
    #         print("Port {} is open".format(port))
    #     s.close()

except KeyboardInterrupt:
    print("\n Exiting Program !!!!")
    sys.exit()
except socket.gaierror:
    print("\n Hostname Could Not Be Resolved !!!!")
    sys.exit()
except socket.error:
    print("\ Server not responding !!!!")
    sys.exit()
